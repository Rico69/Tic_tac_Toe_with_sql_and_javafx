package com.example.database;

import java.sql.Timestamp;

public class GameModel extends BaseEntity {
    public enum Result {
        WIN, ACTIVE, DRAW
    }

    private PlayerModel player1;
    private PlayerModel player2;
    private PlayerModel winner;
    private int boardSize;
    private Result result;
    private Timestamp startTime;
    private Timestamp endTime;
    public GameModel() {
        super();

    }

    public GameModel(PlayerModel player1, PlayerModel player2, int boardSize,Timestamp startTime) {
        super();
        this.player1 = player1;
        this.player2 = player2;
        this.winner = null;
        this.boardSize = boardSize;
        this.result = null;
        this.startTime = startTime;
        this.endTime = null;
    }

    public PlayerModel getPlayer1() {
        return player1;
    }

    public void setPlayer1(PlayerModel player1) {
        this.player1 = player1;
    }

    public PlayerModel getPlayer2() {
        return player2;
    }

    public void setPlayer2(PlayerModel player2) {
        this.player2 = player2;
    }

    public PlayerModel getWinner() {
        return (winner != null) ? winner : null;
    }

    public void setWinner(PlayerModel winner) {
        this.winner = winner;
    }

    public int getBoardSize() {
        return boardSize;
    }
    public Timestamp getStartTime() {
        return this.startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return this.endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    };
    public void setBoardSize(int boardSize) {
        this.boardSize = boardSize;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return super.getId() + " -> {" +
                this.player1.getPlayerName() + ", " + this.player2.getPlayerName() + ", " +
                this.result + ", " + ((this.winner != null) ? this.winner.getPlayerName() : "NO WINNER") + ", " +
                this.boardSize + ", " + this.getStartTime() + ", " +
                this.getEndTime() + "} ";
    }
}