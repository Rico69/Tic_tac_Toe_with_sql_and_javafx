
package com.example.database;

/**
 * SQLCreator
 */
public interface SQLCreator {
    String CreateSql(BaseEntity entity);
}