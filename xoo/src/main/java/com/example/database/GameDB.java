package com.example.database;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.example.database.BaseEntity;
public class GameDB extends BaseDB{

    public GameDB() {
        super();
    }
    @Override
    protected BaseEntity createModel(BaseEntity entity, ResultSet res) throws SQLException {
        if (entity instanceof GameModel) {
            return createModel((GameModel) entity, res);
        }
        return null;
    }


    @Override
    protected BaseEntity newEntity() {
        return new GameModel();
    }


    @Override
    public String createInsertSql(BaseEntity entity) {
        String sqlStr = "";
        if (entity instanceof GameModel) {
            GameModel game = (GameModel) entity;

            sqlStr = "INSERT INTO games (player1, player2, winner, boardSize, result, StartTime, EndTime) VALUES ('"
                    + game.getPlayer1().getPlayerName() + "', '" +
                    game.getPlayer2().getPlayerName() + "', '"
                    + ((game.getWinner() != null ) ? game.getWinner().getPlayerName() : "NO WINNER") + "', " + game.getBoardSize() + ", '" +
                    game.getResult()
                    + "', '" + game.getStartTime() + "', '" + ((game.getEndTime() != null) ? game.getEndTime() : game.getStartTime()) + "')";
            System.out.println(sqlStr);
        }
        return sqlStr;
    }

    @Override
    public String createUpdateSql(BaseEntity entity) {
        String sqlStr = "";
        if (entity instanceof GameModel) {
            GameModel game = (GameModel) entity;
            sqlStr = "UPDATE games SET player1 = '" +
                    game.getPlayer1().getPlayerName() + "', PlayerTwo = '" +
                    game.getPlayer2().getPlayerName() + "', Winner = '" +
                    ((game.getWinner() != null) ? game.getWinner().getPlayerName() : "NO WINNER") + "', BoardSize = " +
                    game.getBoardSize() + ", Result = '" + game.getResult().toString() + "',StartTime = '" +
                    game.getStartTime() + "', EndTime = '" + game.getEndTime() + "' WHERE GameID = " + game.getId();

        }

        return sqlStr;
    }

    @Override
    public String createDeleteSql(BaseEntity entity) {
        String sqlStr = "";
        if (entity instanceof GameModel) {
            GameModel game = (GameModel) entity;
            sqlStr = "DELETE FROM games WHERE gameId = " + game.getId();
        }
        return sqlStr;
    }

    @Override
    public void insert(BaseEntity entity) {
        if (entity instanceof GameModel) {
            inserted.add(new ChangeEntity(entity, this::createInsertSql));
        }
    }

    @Override
    public void update(BaseEntity entity) {
        if (entity instanceof GameModel) {
            updated.add(new ChangeEntity(entity, this::createUpdateSql));
        }
    }

    @Override
    public void delete(BaseEntity entity) {
        if (entity instanceof GameModel) {
            deleted.add(new ChangeEntity(entity, this::createDeleteSql));
        }
    }

    protected GameModel createModel(GameModel game, ResultSet res) throws SQLException {
        if (game instanceof GameModel) {
            GameModel gm = (GameModel) game;
            gm.setId(res.getInt("gameId"));
            String player1Name = res.getString("player1");
            String player2Name = res.getString("player2");
            String winnerName = res.getString("winner");

            try {
                gm.setPlayer1(new PlayerDB().selectPlayerByName(player1Name));
            } catch (IndexOutOfBoundsException e) {
                gm.setPlayer1(null); // Handle case when player is not found
            }

            try {
                gm.setPlayer2(new PlayerDB().selectPlayerByName(player2Name));
            } catch (IndexOutOfBoundsException e) {
                gm.setPlayer2(null); // Handle case when player is not found
            }

            if (winnerName == null || winnerName.isEmpty()) {
                gm.setWinner(null);
            } else {
                try {
                    gm.setWinner(new PlayerDB().selectPlayerByName(winnerName));
                } catch (IndexOutOfBoundsException e) {
                    gm.setWinner(null); // Handle case when winner is not found
                }
            }

            gm.setBoardSize(res.getInt("boardSize"));
            String resultString = res.getString("result");
            GameModel.Result result = GameModel.Result.valueOf(resultString.toUpperCase());
            gm.setResult(result);
            game.setStartTime(res.getTimestamp("StartTime"));
            game.setEndTime(res.getTimestamp("EndTime"));
            return gm;
        }
        return null;

    }

    @Override
    protected BaseDB me() {
        return this;
    }
}
