package com.example.xoo;


import java.sql.Timestamp;
import com.example.database.GameDB;
import com.example.database.GameModel;
import com.example.database.PlayerDB;
import com.example.database.PlayerModel;

public class SqlHelper {
    private final PlayerDB playerDB = new PlayerDB();
    private final GameDB gameDB = new GameDB();
    private PlayerModel playerOne;
    private PlayerModel playerTwo;
    private GameModel gameModel;

    public SqlHelper(String username1, String username2, int boardSize, Timestamp startTime) {
        this.playerOne = new PlayerModel(username1);
        if (this.playerDB.selectByName(username1).isEmpty()) {
            this.playerDB.insert(this.playerOne);
        } else {
            this.playerOne = playerDB.selectByName(username1).get(0);
        }
        this.playerDB.saveChanges();
        this.playerTwo = new PlayerModel(username2);
        if (this.playerDB.selectByName(username2).isEmpty()) {
            this.playerDB.insert(this.playerTwo);
        } else {
            this.playerTwo = playerDB.selectByName(username2).get(0);
        }
        this.playerDB.saveChanges();
        this.gameModel = new GameModel(this.playerOne, this.playerTwo, boardSize,startTime);
        this.gameModel.setResult(GameModel.Result.ACTIVE);
        this.gameDB.insert(gameModel);
        this.gameDB.saveChanges();
    }

    public PlayerModel getPlayerOne() {
        return this.playerOne;
    }

    public PlayerModel getPlayerTwo() {
        return this.playerTwo;
    }

    public void win(PlayerModel winner,Timestamp time) {
        this.gameModel.setResult(GameModel.Result.WIN);
        this.gameModel.setWinner(winner);
        this.gameModel.setEndTime(time);
        this.gameDB.update(this.gameModel);
        this.gameDB.saveChanges();
    }

    public void draw(Timestamp endTime) {
        this.gameModel.setResult(GameModel.Result.DRAW);
        this.gameModel.setEndTime(endTime);

        this.gameDB.update(this.gameModel);
        this.gameDB.saveChanges();
    }
}
