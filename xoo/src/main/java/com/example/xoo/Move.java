package com.example.xoo;

import java.io.Serializable;

public record Move(int x, int y, boolean terminate, boolean win, boolean draw) implements Serializable {
    public Move(int x, int y){this(x, y, false, false, false);}
}